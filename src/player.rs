pub struct Player {
  pub start_hp: i32,
  pub hp: i32,
}

impl Player {
  pub fn new() -> Player {
    Player {
      start_hp: 12,
      hp: 12,
    }
  }

  pub fn deplete_hp(&mut self, value: i32) {
    self.hp -= value;
  }

  pub fn is_dead(&self) -> bool {
    return self.hp == 0;
  }

  pub fn respawn(&mut self) {
    self.hp = self.start_hp;
  }
}
