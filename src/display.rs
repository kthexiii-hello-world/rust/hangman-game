use crossterm::{
  cursor, execute,
  style::{self, Colorize},
  terminal, ExecutableCommand, QueueableCommand, Result,
};
use std::io::{stdout, Write};

pub struct Screen {
  pub buffer: String,
  pub width: u32,
  pub height: u32,
  stdout: std::io::Stdout,
}

impl Screen {
  pub fn new(width: u32, height: u32) -> Screen {
    Screen {
      buffer: String::new(),
      width: width,
      height: height,
      stdout: stdout(),
    }
  }

  pub fn clear_buffer(&mut self) {
    self.buffer = String::new();
  }

  pub fn cursor_to(&mut self, x: u16, y: u16) {
    execute!(self.stdout, cursor::SavePosition, cursor::MoveTo(x, y));
  }

  pub fn reset_cursor(&mut self) {
    self.stdout.execute(cursor::RestorePosition);
  }

  pub fn show(&mut self) {
    self
      .stdout
      .write_all(self.buffer.as_bytes())
      .expect("Error writing");

    self.stdout.flush().expect("Error flushing");
  }

  pub fn clear(&mut self) {
    self.cursor_to(0, 0);
    self
      .stdout
      .execute(terminal::Clear(terminal::ClearType::All))
      .expect("Error clearing the terminal");

    self.stdout.flush().expect("Error flushing")
  }
}
