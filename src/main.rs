use rand::Rng;
use std::io::{stdin, stdout, Write};

mod display;
mod hangman;
mod player;
use display::Screen;
use hangman::Hangman;
use player::Player;

enum GameState {
    Start,
    Playing,
    Pause,
    GameOver,
    Reset,
}

fn yes_or_no() -> bool {
    let mut input = String::new();
    Write::flush(&mut stdout()).expect("Error flushing");
    stdin()
        .read_line(&mut input)
        .ok()
        .expect("Error reading input");
    return input.trim() == "y" || input.trim() == "ye" || input.trim() == "yes";
}

fn main() {
    let mut words = vec![
        "hello",
        "world",
        "monkey",
        "required",
        "random",
        "probability",
        "marker",
        "guarantee",
        "access",
        "need",
        "guarantee",
        "memory",
        "life",
        "related",
        "year",
        "bear",
        "ägg",
    ];

    let mut rng = rand::thread_rng();

    let mut player = Player::new();

    let mut current_state = GameState::Start;

    let mut screen = Screen::new(64, 16);
    let mut hangman = Hangman::new("".to_string());
    let mut is_running = true;

    let mut input = String::new();

    let mut main_text: String = String::new();
    main_text.push_str("┌━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┐\n");
    main_text.push_str("│         Hangman Game         │\n");
    main_text.push_str("└━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┘\n");
    main_text.push_str(" To pause the game type < or _  \n");
    main_text.push_str(" 1. Play (default)              \n");
    main_text.push_str(" 2. Set mystery word            \n");
    main_text.push_str(" 3. Exit                        \n");

    let mut pause_text = String::new();
    pause_text.push_str("┌━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┐\n");
    pause_text.push_str("│         PAUSED               │\n");
    pause_text.push_str("└━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┘\n");
    pause_text.push_str(" 1. Continue (default)          \n");
    pause_text.push_str(" 2. Restart                     \n");
    pause_text.push_str(" 3. Start Menu                  \n");
    pause_text.push_str(" 4. Exit                        \n");

    while is_running {
        match current_state {
            GameState::Start => {
                screen.clear();
                print!("{}", main_text);

                print!("\nInput: ");
                input = hangman.read_input();
                let mut val = 0;
                match input.parse::<u32>() {
                    Ok(i) => val = i,
                    Err(..) => val = 0,
                }

                match val {
                    3 => {
                        is_running = false;
                    }
                    2 => {
                        screen.clear();
                        println!("The word will be converted to lowercase");
                        print!("Set custom word: ");
                        input = hangman.read_input();
                        if !input.is_empty() {
                            input.retain(|c| !c.is_whitespace());
                            hangman.reset(&mut player, input.trim().to_lowercase());
                            current_state = GameState::Playing;
                        } else {
                            println!("Error, word cannot be empty");
                        }
                    }
                    1 | _ => {
                        current_state = GameState::Reset;
                    }
                }
            }
            GameState::Pause => {
                screen.clear();
                print!("{}", pause_text);

                print!("\nInput: ");
                input = hangman.read_input();
                let mut val = 0;
                match input.parse::<u32>() {
                    Ok(i) => val = i,
                    Err(..) => val = 0,
                }

                match val {
                    4 => {
                        is_running = false;
                    }
                    3 => {
                        current_state = GameState::Start;
                    }
                    2 => {
                        current_state = GameState::Reset;
                    }
                    1 | _ => {
                        current_state = GameState::Playing;
                    }
                }
            }
            GameState::Reset => {
                hangman.reset(
                    &mut player,
                    words[rng.gen_range(0, words.len())].to_string(),
                );
                current_state = GameState::Playing;
            }
            GameState::Playing => {
                screen.clear_buffer();
                screen.clear();

                hangman.graphics(&player, &mut screen);
                screen.show();
                println!("HP: {}\n", player.hp);
                hangman.print_board();

                print!("Input: ");
                input = hangman.read_input();

                if input == "<" || input == "_" {
                    current_state = GameState::Pause;
                    continue;
                }

                if input.is_empty() || input.chars().count() < 2 && hangman.check_duplicate(&input)
                {
                    continue;
                }

                if input.chars().count() > 1 {
                    if !hangman.compare_word(&input.to_lowercase()) {
                        player.deplete_hp(input.chars().count() as i32);
                    } else {
                        hangman.game_won();
                    }
                } else if !hangman.compare_chars(&mut input.to_lowercase()) {
                    player.deplete_hp(1);
                }

                if hangman.is_game_completed() {
                    current_state = GameState::GameOver;
                }

                if player.hp <= 0 {
                    player.deplete_hp(1);
                    current_state = GameState::GameOver;
                }
            }
            GameState::GameOver => {
                screen.clear();
                screen.clear_buffer();

                if player.hp < player.start_hp {
                    hangman.graphics(&player, &mut screen);
                    screen.show();
                    println!("HP: {}\n", player.hp);
                }

                if player.hp <= 0 {
                    println!("HP: {}\n", player.hp);
                    println!("You're dead!\n");
                } else {
                    println!("You've won!\n");
                }
                println!("1. Restart (default)");
                println!("2. Start Menu");
                println!("3. Exit");

                print!("\nInput: ");
                input = hangman.read_input();
                let mut val = 0;
                match input.parse::<u32>() {
                    Ok(i) => val = i,
                    Err(..) => val = 0,
                }

                match val {
                    3 => {
                        is_running = false;
                    }
                    2 => {
                        current_state = GameState::Start;
                    }
                    1 | _ => {
                        current_state = GameState::Reset;
                    }
                }
            }
        }
    }
}
