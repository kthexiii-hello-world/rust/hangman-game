use std::io::{stdin, stdout, Write};

use crate::display::Screen;
use crate::player::Player;

pub struct Hangman {
  pub pass_guess: Vec<char>,
  pub word_board: String,
  pub current_word: String,
}

impl Hangman {
  pub fn new(current_word: String) -> Hangman {
    Hangman {
      pass_guess: Vec::new(),
      word_board: Hangman::init_board(current_word.clone()),
      current_word: current_word.clone(),
    }
  }

  pub fn reset(&mut self, player: &mut Player, new_word: String) {
    player.respawn();
    self.current_word = new_word.clone();
    self.word_board = Hangman::init_board(new_word.clone());
    self.pass_guess = Vec::new();
  }

  fn init_board(word: String) -> String {
    let mut tmp = String::new();
    for _ in 0..word.chars().count() {
      tmp.push('_');
    }
    return tmp;
  }

  pub fn game_won(&mut self) {
    self.word_board = self.current_word.clone();
  }

  pub fn is_game_completed(&self) -> bool {
    return self.current_word == self.word_board;
  }

  pub fn read_input(&self) -> String {
    let mut value = String::new();
    Write::flush(&mut stdout()).expect("Error flushing");
    stdin()
      .read_line(&mut value)
      .ok()
      .expect("Error reading input");
    return value.trim().to_owned();
  }

  pub fn print_board(&self) {
    print!("Word: ");
    for c in self.word_board.chars() {
      if c == '_' {
        print!("_ ");
      } else {
        print!("{}", c);
      }
    }
    print!("\n\n");
  }

  pub fn check_duplicate(&mut self, character: &String) -> bool {
    for c in self.pass_guess.to_owned() {
      if c == character.chars().nth(0).expect("Error getting char") {
        return true;
      }
    }

    return false;
  }

  pub fn compare_chars(&mut self, character: &mut String) -> bool {
    let mut tof = false;
    let mut board = String::new();

    if character == "" {
      return false;
    }

    for i in 0..self.current_word.chars().count() {
      let cc = self
        .current_word
        .chars()
        .nth(i)
        .expect("Error getting char");
      let bc = self.word_board.chars().nth(i).expect("Error getting char");

      let ic = character.chars().nth(0).expect("Error getting char");
      if ic == cc {
        tof = true;
        board.push(ic);
      } else if bc == cc {
        board.push(bc);
      } else {
        board.push('_');
      }
    }

    if !tof {
      for j in 0..character.chars().count() {
        let ic = character.chars().nth(j).expect("Error");
        self.pass_guess.push(ic);
      }
    }

    self.word_board = board;
    return tof;
  }

  pub fn compare_word(&self, word: &String) -> bool {
    return *word == self.current_word.to_string();
  }

  fn sprite(&self) -> String {
    let mut s = String::new();
    s.push_str("   _________ \n");
    s.push_str("  │ ╱      | \n");
    s.push_str("  │╱       0 \n");
    s.push_str("  │       /|\\\n");
    s.push_str("  │       / \\\n");
    s.push_str("  │╲         \n");
    s.push_str("──┴──        \n");

    return s;
  }

  fn stages(&self) -> Vec<u32> {
    return vec![
      0x0, 0x0, 0xB, 0x9, 0x9, 0x9, 0x9, 0x9, 0x9, 0x9, 0x9, 0x9, 0x9, 0x0, 0x0, 0x0, 0xB, 0x0,
      0x8, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x7, 0x0, 0x0, 0x0, 0x0, 0xB, 0x8, 0x0, 0x0, 0x0, 0x0,
      0x0, 0x0, 0x0, 0x6, 0x0, 0x0, 0x0, 0x0, 0xB, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x4, 0x5,
      0x3, 0x0, 0x0, 0x0, 0xB, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x2, 0x0, 0x1, 0x0, 0x0, 0x0,
      0xB, 0xA, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0xC, 0xC, 0xC, 0xC, 0xC, 0x0,
      0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0,
    ];
  }

  fn show_guessed(&mut self) {
    print!("\nletters: ");
    for i in self.pass_guess.iter() {
      print!("{}, ", i);
    }
    print!("\n");
  }

  pub fn graphics(&mut self, player: &Player, screen: &mut Screen) {
    self.show_guessed();

    for i in 0..self.sprite().chars().count() {
      let c = self.sprite().chars().nth(i).expect("Error getting char");
      let s = self.stages()[i];

      if player.hp < (s as i32) {
        screen.buffer.push(c)
      } else {
        if c == '\n' {
          screen.buffer.push('\n');
        } else {
          screen.buffer.push(' ');
        }
      }
    }
  }
}
